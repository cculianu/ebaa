awk -F ',' '{OFS=","; print $1,$3,$3}' $1 \
| ../../implementation-c/bin/aggregate -windowlength 4 \
| awk -F ',' 'NR>1 {OFS=","; print NR-2,0,$15}' \
> chunksizes-ltc.csv
